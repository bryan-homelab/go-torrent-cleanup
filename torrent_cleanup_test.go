package main

import (
	"reflect"
	"testing"

	"github.com/odwrtw/transmission"
)

func TestGetTorrents(t *testing.T) {
	conf := transmission.Config{
		Address: "http://10.10.10.120:10109/transmission/rpc/",
	}

	client, err := transmission.New(conf)
	handle_err(err)
	torrents := get_torrents(client, true)

	if len(torrents) == 0 {
		t.Skip()
	} else {
		if reflect.TypeOf(torrents[0]).String() != "*transmission.Torrent" {
			t.Error("Torrent list does not contain a list of transmission.Torrent")
		}
	}
}

func TestRemoveTorrents(t *testing.T) {
	conf := transmission.Config{
		Address: "http://10.10.10.120:10109/transmission/rpc/",
	}

	client, err := transmission.New(conf)
	handle_err(err)

	args := transmission.AddTorrentArg{
		Filename: "https://cdimage.debian.org/debian-cd/current/amd64/bt-cd/debian-11.6.0-amd64-netinst.iso.torrent",
		Paused:   true,
	}

	new_torrent, err := client.AddTorrent(args)

	handle_err(err)

	t_list := make([]*transmission.Torrent, 0)

	t_list = append(t_list, new_torrent)

	remove_torrents(client, t_list, true)

	current_torrents := get_torrents(client, true)

	for i := 0; i < len(current_torrents); i++ {
		if new_torrent.Name == current_torrents[i].Name {
			t.Error("Test Torrent was not deleted")
		}
	}
}
