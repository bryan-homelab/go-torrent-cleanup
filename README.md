# go-torrent-cleanup

This is a go script to remove any stale torrents from your Transmission instance. 

## Usage

```
 -host string
        Host name or IP address of whats hosting Transmission (default "10.10.10.120")
  -port string
        Port of your transmission instance (default "9091")
  -remove-data
        Remove data when deleting the torrent
```