package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/odwrtw/transmission"
)

func get_torrents(t *transmission.Client, return_all bool) []*transmission.Torrent {

	torrents_to_remove := make([]*transmission.Torrent, 0)

	torrents, err := t.GetTorrents()

	if err != nil {
		fmt.Println("Error getting torrents")
		fmt.Println(err)
	}

	for i := 0; i < len(torrents); i++ {
		tor := torrents[i]

		if return_all {
			torrents_to_remove = append(torrents_to_remove, tor)
		} else {
			if tor.IsStalled {
				torrents_to_remove = append(torrents_to_remove, tor)
			}
		}
	}

	return (torrents_to_remove)
}

func remove_torrents(t *transmission.Client, torrents []*transmission.Torrent, remove_data bool) {

	t.RemoveTorrents(torrents, remove_data)
}

func handle_err(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func main() {

	host := flag.String("host", "10.10.10.120", "Host name or IP address of whats hosting Transmission")
	port := flag.String("port", "9091", "Port of your transmission instance")
	removeData := flag.Bool("remove-data", false, "Remove data when deleting the torrent")

	flag.Parse()

	args := fmt.Sprintf("Connecting to %s on port %s.\nRemoving data: %s", *host, *port, strconv.FormatBool(*removeData))
	fmt.Println(args)
	conf := transmission.Config{
		Address: "http://" + *host + ":" + *port + "/transmission/rpc/",
	}

	client, err := transmission.New(conf)

	handle_err(err)

	torrents := get_torrents(client, false)

	if len(torrents) == 0 {
		fmt.Println("No torrents that need to be removed, exiting")
		os.Exit(0)
	}

	fmt.Println("Following torrents will be removed from transmission and deleted from the filesystem")

	for i := 0; i < len(torrents); i++ {
		fmt.Println(torrents[i].Name)
	}

	remove_torrents(client, torrents, *removeData)
}
